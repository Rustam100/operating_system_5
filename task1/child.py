#!/usr/bin/python

import sys
import os
import time
import random


sleep_time = sys.argv[1]
sleep_time = int(sleep_time)
id = os.getpid()
parent_id = os.getppid()

print(f"Child [{id}]: I am started. PID {id}. Parent PID {parent_id}")

time.sleep(sleep_time)

print(f"Child [{id}]: I am ended. PID {id}. Parent PID {parent_id}")

status = random.randint(0, 1)
sys.exit(status)