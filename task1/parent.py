#!/usr/bin/python

import os
import sys
import random


def run_children():
    child = os.fork()
    if child == 0:
        argument = str(random.randint(5, 10))
        os.execl("./child.py", "child.py", argument)
    print(f"Parent [{os.getpid()}]: I ran children process with PID {child}")


num = sys.argv[1]
num = int(num)

for i in range(0, num):
    run_children()

while num > 0:
    pid, status = os.wait()
    status = status / 256
    status = int(status)
    print(f"Parent[{os.getpid()}]: Child with PID {pid} terminated. Exit Status {status}.")
    if status == 0:
        num = num - 1
    else:
        run_children()